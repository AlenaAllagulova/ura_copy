<?php

class Plugin_Payboxmoney_p16c2dd extends Plugin
{
    const ID  = 512;
    const KEY = 'payboxmoney';

    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'plugin_title'   => 'Шлюз оплаты "Paybox.Money"',
            'plugin_version' => '1.0.0',
            'extension_id'   => 'p16c2dd8e401bb35d058186838f0c2d02af97f7e',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            'id' => array(
                'title' => 'Идентификатор проекта paybox.money',
                'input' => 'text',
            ),
            'secret' => array(
                'title' => 'Секретный ключ для приема платежей',
                'input' => 'password',
            ),
            'client_secret' => array(
                'title' => 'Секретный ключ для выплат клиентам',
                'input' => 'password',
            ),
            'test_mode' => array(
                'type'  => TYPE_BOOL,
                'title' => 'Тестовый режим',
                'input' => 'checkbox',
            ),
            'currency' => array(
                'title' => 'Валюта',
                'input' => 'select',
                'type'  => TYPE_STR,
                'default' => 'RUB',
                'options' => array(
                    'KZT' => array('title' => 'тенге'),
                    'USD' => array('title' => 'united states dollar'),
                    'EUR' => array('title' => 'euro'),
                ),
            ),
            'process_url' => array(
                'title' => 'Страница обработки оплаты',
                'input' => 'text',
                'readonly' => true,
                'default' => Bills::url('process', array('ps'=>static::KEY)),
            ),
            'success_url' => array(
                'title' => 'Страница успешной оплаты',
                'input' => 'text',
                'readonly' => true,
                'default' => Bills::url('success'),
            ),
            'fail_url' => array(
                'title' => 'Страница ошибки оплаты',
                'input' => 'text',
                'readonly' => true,
                'default' => Bills::url('fail'),
            ),
        ));
    }

    /**
     * Запуск плагина
     */
    protected function start()
    {
        bff::hookAdd('bills.pay.systems.user', array($this, 'user_list'));
        bff::hookAdd('bills.pay.systems.data', array($this, 'system_list'));
        bff::hookAdd('bills.pay.form', array($this, 'form'));
        bff::hookAdd('bills.pay.process', array($this, 'process'));
    }

    /**
     * Дополняем список доступных пользователю способов оплаты
     * @param array $list список систем оплат
     * @param array $extra: 'logoUrl', 'balanceUse'
     * @return array
     */
    public function user_list($list, $extra)
    {
        $list['payanyway_1'] = array(
            'id'           => static::ID,
            'logo_desktop' => $this->url('/paybox_money.png'),
            'logo_phone'   => $this->url('/paybox_money.png'),
            'way'          => '',
            'title'        => _t('bills', 'PayboxMoney'), # Название способа оплаты
            'currency_id'  => Site::currencyDefault('id'), # Тэнге (ID валюты в системе)
            'enabled'      => true, # Способ доступен пользователю
            'priority'     => 0, # Порядок: 0 - последняя, 1+
        );
        return $list;
    }

    /**
     * Дополняем данными о системе оплаты
     * @param array $list
     * @return array
     */
    public function system_list($list)
    {
        $list[static::ID] = array(
            'id'    => static::ID,
            'key'   => static::KEY,
            'title' => 'PayboxMoney', # Название системы для описания счета в админ. панели
            'desc'  => '',
        );
        return $list;
    }

    /**
     * Форма выставленного счета, отправляемая системе оплаты
     * @param string $form HTML форма
     * @param integer $paySystem ID системы оплаты для которой необходимо сформировать форму
     * @param array $data дополнительные данные о выставляемом счете:
     *  amount - сумма для оплаты
     *  bill_id - ID счета
     *  bill_description - описание счета
     * @return string HTML
     */
    public function form($form, $paySystem, $data)
    {
        if ($paySystem != static::ID) return $form;

        $PG_ID = $this->config('id');
        $PG_SALT = rand(21,43433);
        $PG_TRANSACTION_ID = $data['bill_id'];
        $PG_AMOUNT = number_format($data['amount'], 2, '.', '');
        $PG_CURRENCY_CODE = $this->config('currency');
        $PG_DESCRIPTION = $data['bill_description'];

        $params = [
            'pg_merchant_id' => $PG_ID,
            'pg_order_id' => $PG_TRANSACTION_ID,
            'pg_amount' => $PG_AMOUNT,
            'pg_description' => $PG_DESCRIPTION,
            'pg_currency' => $PG_CURRENCY_CODE,
            'pg_success_url' =>  $this->config('success_url'),
            'pg_result_url' =>  $this->config('process_url'),
            'pg_failure_url' =>  $this->config('fail_url'),
            'pg_salt' => $PG_SALT,
            'pg_request_method' => 'POST',
            'pg_testing_mode' => $this->config('test_mode')
        ];

        ksort($params);

        $PG_SIGNATURE = $this->makeSign($params);

        $params['pg_sig'] = $PG_SIGNATURE;

        ksort($params);

        return $this->formCreate($params);
    }

    /**
     * Обработка запроса от системы оплаты
     * Метод должен завершать поток путем вызова bff::shutdown();
     * @param string $system ключ обрабываемой системы оплаты
     */
    public function process($system)
    {
        if ($system != static::KEY) return;

        $secret = $this->config('secret');
        if (empty($secret)) {
            $this->log('Не указана настройка Secret');
            return;
        }

        $aRequest = $_POST;
        unset($aRequest['s']);
        unset($aRequest['ev']);

        $pg_sig = $this->getValue('pg_sig');
        $pg_salt = $this->getValue('pg_salt');
        $pg_amount = $this->getValue('pg_amount');
        $pg_can_reject = $this->getValue('pg_can_reject');
        $pg_order_id = $this->getValue('pg_order_id');
        $pg_result = $this->getValue('pg_result');

        if (empty($pg_sig) || !self::checkSign($pg_sig, 'payboxmoney', $aRequest, $secret)) {
            $this->log('wrong signature');
            die("Wrong signature");
        }
        if(empty($pg_result))
            die('Payment error');

        $bResult = $this->bills()->processBill($pg_order_id, $pg_amount, self::ID);
        $strResponseStatus = 'ok';
        $strResponseDescription = "Оплата принята";
        if(empty($bResult))
            if($pg_can_reject == 1)
                $strResponseStatus = 'rejected';
            else
                $strResponseStatus = 'error';

        $objResponse = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><response/>');
        $objResponse->addChild('pg_salt', $pg_salt);
        $objResponse->addChild('pg_status', $strResponseStatus);
        $objResponse->addChild('pg_description', $strResponseDescription);
        $objResponse->addChild('pg_sig', self::makeXML('payboxmoney', $objResponse, $secret));

        header("Content-type: text/xml");
        echo $objResponse->asXML();
        bff::shutdown();
    }

    protected function getValue($name)
    {
        $value = false;
        if (isset($_POST[$name])) $value = $_POST[$name];
        if (isset($_GET[$name])) $value = $_GET[$name];
        return $value;
    }

    public function formCreate($aParams) {
        $form = '<form action="https://www.paybox.kz/payment.php" method="POST">';
        foreach ($aParams as $key => $param) {
            $form .=  '<input type="hidden" name="' . $key . '" value="' . $param . '" />';
        }
        $form .= '</form>';
        return $form;
    }

    /**
     * Returns flat array of XML params
     *
     * @param (string|SimpleXMLElement) $xml
     * @return array
     */
    private static function makeFlatParamsXML ($xml, $parent_name = '')
    {
        if ( ! $xml instanceof SimpleXMLElement ) {
            $xml = new SimpleXMLElement($xml);
        }
        $arrParams = array();
        $i = 0;
        foreach ( $xml->children() as $tag ) {
            $i++;
            if ( 'pg_sig' == $tag->getName() )
                continue;
            $name = $parent_name . $tag->getName().sprintf('%03d', $i);
            if ( $tag->children()->count() > 0) {
                $arrParams = array_merge($arrParams, self::makeFlatParamsXML($tag, $name));
                continue;
            }
            $arrParams += array($name => (string)$tag);
        }
        return $arrParams;
    }

    /**
     * make the signature for XML
     *
     * @param $sScriptName
     * @param string|SimpleXMLElement $xml
     * @param string $strSecretKey
     * @return string
     */
    public static function makeXML ($sScriptName, $xml, $strSecretKey)
    {
        $arrFlatParams = self::makeFlatParamsXML($xml);
        return self::sign($sScriptName, $arrFlatParams, $strSecretKey);
    }

    public static function getScriptNameFromUrl($url)
    {
        $path = parse_url($url, PHP_URL_PATH);
        $len  = strlen($path);
        if ( $len == 0  ||  '/' == $path{$len-1} ) {
            return "";
        }
        return basename($path);
    }

    public static function getScriptName()
    {
        return self::getScriptNameFromUrl($_SERVER['PHP_SELF']);
    }

    private static function makeFlatParamsArray ($arrParams, $parent_name = '')
    {
        $aFlatParams = array();
        $i = 0;
        foreach ( $arrParams as $key => $val ) {
            $i++;
            if ('pg_sig' == $key)
                continue;
            $name = $parent_name . $key . sprintf('%03d', $i);
            if (is_array($val) ) {
                $aFlatParams = array_merge($aFlatParams, self::makeFlatParamsArray($val, $name));
                continue;
            }
            $aFlatParams += array($name => (string)$val);
        }

        return $aFlatParams;
    }

    private static function makeSigStr($sScriptName, array $aParams, $sSecretKey) {
        unset($aParams['pg_sig']);
        ksort($aParams);
        array_unshift($aParams, $sScriptName);
        array_push   ($aParams, $sSecretKey);
        return join(';', $aParams);
    }

    public static function sign($sScriptName, array $aParams, $sSecretKey)
    {
        $aFlatParams = self::makeFlatParamsArray($aParams);
        return md5(self::makeSigStr($sScriptName, $aFlatParams, $sSecretKey));
    }

    /**
     * Verifies the signature
     *
     * @param string $signature
     * @param $sScriptName
     * @param array $Params associative array of parameters for the signature
     * @param string $sSecretKey
     * @return bool
     */
    public static function checkSign($signature, $sScriptName, array $Params, $sSecretKey)
    {
        return (string)$signature === self::sign($sScriptName, $Params, $sSecretKey);
    }

    public function makeSign(array $aParams)
    {
        return self::sign(
            "payment.php",
            $aParams,
            $this->config('secret')
        );
    }

    /**
     * Логирование ошибок
     * @param string $message текст ошибки
     * @return mixed
     */
    public function log($message, $level = Logger::ERROR, array $context = ['bills.log'])
    {
        return bff::log('Paybox: '.$message, $level, $context);
    }
}