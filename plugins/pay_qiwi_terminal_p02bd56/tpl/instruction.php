<?php
/**
 * @var $this Plugin_Pay_Qiwi_Terminal_p02bd56
 * @var $user_id integer ID пользователя
 * @var $amount float Сумма оплаты
 * @var $currency string Валюта оплаты
 * @var $pay_data array Дополнительные данные с учетом конвертации по курсу
 */
?>
<div class="text-center">
    <?= $this->lang('Для пополнения счета системой Qiwi, укажите при пополнении<br />через терминал следующий номер вашего счета:'); ?>
    <br /><br />
    <div>
        <strong># <?= $user_id ?></strong>
    </div>
</div>