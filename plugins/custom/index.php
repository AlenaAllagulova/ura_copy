<?php

class Plugin_Custom extends Plugin
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'plugin_title'   => 'Доработки',
            'plugin_version' => '1.0.0',
        ));

        $this->configSettings(array(
            // Настройки плагина
        ));
    }

    protected function start()
    {
        // Код плагина
        bff::hooks()->billsPaySystemsUser(array($this, 'bills_pay_systems_list'));
    }

    /**
     * Формирование списка доступных пользователю способов оплаты
     * @param array $list список систем оплат
     * @param array $extra: 'logoUrl', 'balanceUse'
     * @return array
     */
    public function bills_pay_systems_list($list, $extra)
    {
        $change = array(
            'robox' => array(
                //'enabled' => false,
            ),
            'wm' => array(
                //'enabled' => false,
            ),
            'terminal' => array(
                //'enabled' => false,
            ),
            'paypal' => array(
                //'enabled' => false,
            ),
            'liqpay' => array(
                //'enabled' => false,
            ),
            'yandex' => array(
                //'enabled' => false,
            ),
            'yandexAC' => array(
                //'enabled' => false,
            ),
            'yandexMC' => array(
                //'enabled' => false,
            ),
        );
        foreach ($change as $k=>$v) {
            if (isset($list[$k]) && !empty($v)) {
                foreach ($v as $kk=>$vv) {
                    $list[$k][$kk] = $vv;
                }
            }
        }

        return $list;
    }
}