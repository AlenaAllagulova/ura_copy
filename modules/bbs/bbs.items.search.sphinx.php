<?php

/**
 * Поиск объявлений при помощи SphinxQL
 */
class BBSItemsSearchSphinx_ extends \bff\utils\Sphinx
{
    public static function enabled()
    {
        return config::sys('bbs.search.sphinx', false, TYPE_BOOL) && parent::enabled();
    }

    /**
     * Поиск объявлений
     * @param string $query строка поиска
     * @param array $filter дополнительные фильтры
     * @param boolean $count только подсчет кол-ва
     * @param integer $limit лимит результатов на страницу
     * @param integer $offset смещение вывода результатов
     * @return array|integer
     */
    public function searchItems($query, $filter, $count = false, $limit = 1, $offset = 0)
    {
        $options = array('field_weights' => '(title=60,descr=40)');
        $query = $this->prepareSearchQuery($query, $options);

        $bind = array(':q' => $query);
        $where = array();
        $select = array();

        # статус
        if (isset($filter['status'])) {
            $where[] = '`status` = :status';
            $bind[':status'] = (int)$filter['status'];
        }

        # тип: продам, куплю
        if (isset($filter['cat_type'])) {
            $where[] = 'cat_type = :cat_type';
            $bind[':cat_type'] = (int)$filter['cat_type'];
        }

        # модерация
        if (isset($filter[':mod'])) {
            $where[] = 'moderated > 0';
        }

        # категория
        for ($i = 1; $i <= BBS::CATS_MAXDEEP; $i++) {
            if (isset($filter['cat_id'.$i])) {
                $where[] = 'cat_id'.$i.' = :cat_id'.$i;
                $bind[':cat_id'.$i] = (int)$filter['cat_id'.$i];
            }
        }

        # регион
        if (isset($filter[':region-filter'])) {
            $regionID = $filter[':region-filter'];
            if ($regionID > 0) {
                $regionData = Geo::regionData($regionID);
                if (config::sysAdmin('bbs.search.delivery', true, TYPE_BOOL)) {
                    if (Geo::coveringType(Geo::COVERING_COUNTRIES)) {
                        switch ($regionData['numlevel']) {
                            case Geo::lvlCountry:
                                $select[] = '(reg1_country = :reg1_country OR (reg1_country = :reg1_country AND regions_delivery = 1)) AS f_deliv';
                                $bind[':reg1_country'] = (int)$regionID;
                                break;
                            case Geo::lvlRegion:
                                $select[] = '(reg2_region = :reg2_region OR (reg1_country = :reg1_country AND regions_delivery = 1)) AS f_deliv';
                                $bind[':reg1_country'] = (int)$regionData['country'];
                                $bind[':reg2_region'] = (int)$regionID;
                                break;
                            case Geo::lvlCity:
                                $select[] = '(reg3_city = :reg3_city OR (reg1_country = :reg1_country AND regions_delivery = 1)) AS f_deliv';
                                $bind[':reg1_country'] = (int)$regionData['country'];
                                $bind[':reg3_city'] = (int)$regionID;
                                break;
                        }
                    } else {
                        switch ($regionData['numlevel']) {
                            case Geo::lvlCountry:
                                $select[] = '(reg1_country = :reg1_country OR regions_delivery = 1) AS f_deliv';
                                $bind[':reg1_country'] = (int)$regionID;
                                break;
                            case Geo::lvlRegion:
                                $select[] = '(reg2_region = :reg2_region OR regions_delivery = 1) AS f_deliv';
                                $bind[':reg2_region'] = (int)$regionID;
                                break;
                            case Geo::lvlCity:
                                $select[] = '(reg3_city = :reg3_city OR regions_delivery = 1) AS f_deliv';
                                $bind[':reg3_city'] = (int)$regionID;
                                break;
                        }
                    }
                    $where[] = 'f_deliv = 1';
                } else {
                    switch ($regionData['numlevel']) {
                        case Geo::lvlCountry:
                            $where[] = 'reg1_country = :reg1_country';
                            $bind[':reg1_country'] = (int)$regionID;
                            break;
                        case Geo::lvlRegion:
                            $where[] = 'reg2_region = :reg2_region';
                            $bind[':reg2_region'] = (int)$regionID;
                            break;
                        case Geo::lvlCity:
                            $where[] = 'reg3_city = :reg3_city';
                            $bind[':reg3_city'] = (int)$regionID;
                            break;
                    }
                }
            }
            unset($filter[':region-filter']);
        } else {
            foreach (array('reg1_country', 'reg2_region', 'reg3_city') as $v) {
                if (isset($filter[$v])) {
                    $where[] = $v.' = :'.$v;
                    $bind[':'.$v] = (int)$filter[$v];
                }
            }
        }

        # с фото
        if (in_array('imgcnt > 0', $filter)) {
            $where[] = 'imgcnt > 0';
        }

        # тип владельца, ID района, ID метро
        foreach (array('owner_type', 'district_id', 'metro_id') as $v) {
            if (isset($filter[$v])) {
                if (is_array($filter[$v])) {
                    $where[] = $this->db->prepareIN($v, $filter[$v]);
                } else {
                    $where[] = $v.' = :'.$v;
                    $bind[':'.$v] = (int)$filter[$v];
                }
            }
        }

        # цена
        if (isset($filter[':price'])) {
            $price = str_replace('I.', '', $filter[':price']);
            $select[] = $price.' AS f_price';
            $where[] = 'f_price = 1';
        }

        # дин. св-ва
        if ( ! empty($filter[':dp']) && is_array($filter[':dp'])) {
            $i = 1;
            foreach ($filter[':dp'] as $v) {
                $v = str_replace('I.', '', $v);
                if (strpos($v, '&') || strpos($v, 'OR')) {
                    $select[] = $v.' AS f_dp'.$i;
                    $where[] = 'f_dp'.$i.' = 1';
                    $i++;
                } else {
                    $where[] = $v;
                }
            }
        }

        $opt = array();
        foreach ($options as $k => $v) {
            $opt[] = $k.'='.$v;
        }

        $order = '';
        if (!$count) {
            $select[] = 'svc_fixed, WEIGHT() AS w';
            $order = ' ORDER BY svc_fixed DESC, w DESC';
        }

        # http://sphinxsearch.com/docs/current/sphinxql-select.html
        $prefix = config::sysAdmin('sphinx.prefix', '', TYPE_STR);
        $data = $this->exec('
            SELECT id '.(!empty($select) ? ', '.join(', ', $select) : '').'
            FROM '.$prefix.'itemsIndexMain, '.$prefix.'itemsIndexDelta
            WHERE MATCH (:q) '.(!empty($where) ? ' AND '.join(' AND ', $where) : '').'
            '.$order.'
            LIMIT '.($offset ? $offset.',' : '').$limit.'
            '.(!empty($opt) ? 'OPTION '.join(',', $opt) : ''),
            $bind, PDO::FETCH_COLUMN, 'fetchAll');
        if ($count) {
            # только подсчет кол-ва
            # http://khaletskiy.blogspot.com/2014/06/sphinx-pagination.html
            $meta = $this->select('SHOW META');
            foreach ($meta as $v) {
                if ($v['Variable_name'] == 'total') {
                    return $v['Value'];
                }
            }
        }

        return $data;
    }
}