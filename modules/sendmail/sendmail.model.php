<?php

# Работа с почтой - массовая рассылка (sendmail)
define('TABLE_MASSEND',           DB_PREFIX . 'massend');
define('TABLE_MASSEND_RECEIVERS', DB_PREFIX . 'massend_receivers');

class SendmailModel_ extends SendmailModelBase
{

    /**
     * Формируем задание на рассылку и заполняем список получателей
     * @param array $aSettings - данные для рассылки
     * @return bool
     */
    public function massendStart($aSettings)
    {
        $shopOnly = ! empty($aSettings['shop_only']);

        if(empty($aSettings)) return false;
        $nReceiversTotal = $this->db->one_data('
            SELECT COUNT(*) FROM ' . TABLE_USERS . '
            WHERE (enotify & ' . Users::ENOTIFY_NEWS . ') AND enotify > 0 AND blocked = 0 AND activated = 1
        '.($shopOnly ? ' AND shop_id > 0 ' : ''));
        $nMassendID = $this->db->insert(TABLE_MASSEND, array(
                'total'    => $nReceiversTotal,
                'started'  => $this->db->now(),
                'settings' => serialize($aSettings),
            )
        );
        if (empty($nMassendID)) {
            return false;
        }
        # сохраняем ID получателей(пользователей) в базу
        $this->db->exec('
            INSERT INTO '.TABLE_MASSEND_RECEIVERS.'(massend_id, user_id)
            SELECT :massend_id, user_id FROM ' . TABLE_USERS . '
            WHERE (enotify & ' . Users::ENOTIFY_NEWS . ') AND enotify > 0 AND blocked = 0 AND activated = 1 '.
                  ($shopOnly ? ' AND shop_id > 0 ' : '').'
            ORDER BY user_id ASC ', array(':massend_id' => $nMassendID));
        return true;
    }

    /**
     * Первая открытая рассылка
     * @return mixed
     */
    public function lastedMassend()
    {
        return $this->db->one_array('SELECT * FROM ' . TABLE_MASSEND . ' WHERE status = 0 ORDER BY finished LIMIT 1');
    }

    /**
     * Список получателей для рассылки SELECT ... FOR UPDATE
     * @param integer $massendID ID рассылки
     * @return mixed
     */
    public function massendReceivers($massendID)
    {
        if(empty($massendID)) return array();

        $limit = config::sysAdmin('sendmail.massend.step', 100, TYPE_UINT);
        $users = $this->db->select_key('
            SELECT user_id FROM ' . TABLE_MASSEND_RECEIVERS . '
            WHERE massend_id = :id AND success = 0
            LIMIT :lim
            FOR UPDATE', 'user_id', array(':id' => $massendID, ':lim' => $limit));
        
        if(empty($users)) return array();

        $receivers = $this->db->select_key('
            SELECT user_id as id, name, surname, email, login
            FROM ' . TABLE_USERS . '
            WHERE user_id IN (' . join(',', array_keys($users)) . ')', 'id');

        foreach($receivers as & $v){
            if(isset($users[ $v['id'] ])){
                $v += $users[ $v['id'] ];
            }
        } unset($v);
        return $receivers;
    }

    /**
     * Обновления статуса получателей в рассылке
     * @param integer $massendID ID рассылки
     * @param array $receivers массив с ID получателей
     */
    public function updateReceivers($massendID, $receivers)
    {
        if(empty($massendID)) return;
        if(empty($receivers)) return;

        $this->db->update(TABLE_MASSEND_RECEIVERS, array(
            'success' => 1,
        ), array(
                'massend_id' => $massendID,
                'user_id' => $receivers,
            )
        );
    }

    /**
     * Закрытие рассылки
     * @param $massendID
     * @param bool|false $force
     */
    public function closeMassend($massendID, $force = false)
    {
        if(empty($massendID)) return;
        $data = $this->db->one_array('SELECT * FROM ' . TABLE_MASSEND . ' WHERE id = :id LIMIT 1', array(':id' => $massendID));
        if(empty($data)){
            $this->db->exec('DELETE FROM '.TABLE_MASSEND_RECEIVERS.' WHERE massend_id = :id', array(':id' => $massendID));
            return;
        }

        $receivers = $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_MASSEND_RECEIVERS.' WHERE massend_id = :id', array(':id' => $massendID));
        if(empty($receivers)){
            $this->db->exec('DELETE FROM '.TABLE_MASSEND.' WHERE id = :id', array(':id' => $massendID));
            return;
        }

        $success = $this->db->one_data('SELECT COUNT(*) FROM '.TABLE_MASSEND_RECEIVERS.' WHERE massend_id = :id AND success = 1', array(':id' => $massendID));
        if($receivers == $success || $force){
            $this->db->update(TABLE_MASSEND, array(
                'status'   => 1,
                'finished' => $this->db->now(),
                'success'  => $success,
                'fail'     => $data['total'] - $success,
            ), array('id' => $massendID));
        }else{
            $this->db->update(TABLE_MASSEND, array(
                'success'  => $success,
                'finished' => $this->db->now(),
            ), array('id' => $massendID));
        }
    }

}