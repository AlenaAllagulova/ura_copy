<?php

class Sendmail_ extends SendmailBase
{
    /**
     * Cron: Массовая рассылка писем
     * - рекомендуемая периодичность: каждые 2-5 минут
     */
    public function cronMassend()
    {
        if (!bff::cron()) {
            return;
        }

        $this->db->begin(); # для LOCK записей таблицы получателей рассылки

        do {
            # получаем информацию об открытой рассылке
            $massend = $this->model->lastedMassend();
            if (empty($massend['id'])) {
                break;
            }
            $massendID = $massend['id'];

            $aSettings = func::unserialize($massend['settings']);
            if ($aSettings === false || empty($aSettings)) {
                $this->errors->set('corrupted massend-settings data (id=' . $massendID . ')');
                $this->model->closeMassend($massendID, true);
                break;
            }

            # формируем текст письма на основе шаблона "massend"
            $isHTML = (is_null($aSettings['is_html']) ? NULL : !empty($aSettings['is_html']));
            $wrapperID = (isset($aSettings['wrapper_id']) ? !empty($aSettings['wrapper_id']) : 0);
            $aTemplate = $this->getMailTemplate('sendmail_massend', array('msg' => $aSettings['body']), LNG, $isHTML, $wrapperID);
            $aSettings['body'] = $aTemplate['body'];
            if (is_array($aSettings['body'])) {
                $msg = $aSettings['body'][LNG];
            } else {
                $msg = $aSettings['body'];
            }

            // SELECT ... FOR UPDATE (mysql)
            $receivers = $this->model->massendReceivers($massendID);
            if (empty($receivers)) {
                $this->model->closeMassend($massendID, true);
                break;
            }
            if (BFF_DEBUG) {
                bff::log('massend started: ' . sizeof($receivers) . ' receivers', Logger::INFO, 'cron.log');
            }

            $result = array();
            $nSuccess = 0;
            foreach ($receivers as $v)
            {
                $replace = array('{fio}' => $v['name']);
                $subject = strtr((is_array($aSettings['subject']) ? $aSettings['subject'][LNG] : $aSettings['subject']), $replace);
                $body = strtr($msg, $replace);
                $from = (!empty($aSettings['from']) ? $aSettings['from'] : '');
                $fromName = (isset($aSettings['fromname']) ? $aSettings['fromname'] : '');
                $customHeaders = array(
                    'Precedence' => 'bulk', # индикатор массовой рассылки для Google
                );
                $res = $this->sendMail($v['email'], $subject, $body, $from, $fromName, $customHeaders);
                if ($res) {
                    $nSuccess++;
                    $result[] = $v['id'];
                }
            }

            if ( ! empty($result)) {
                $this->model->updateReceivers($massendID, $result);
            }

            $this->model->closeMassend($massendID);

            if (BFF_DEBUG) {
                bff::log('massend finish: ' . $nSuccess, Logger::INFO, 'cron.log');
            }

        }while(false);
        $this->db->commit();
    }


    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {

        return array(
            'cronMassend' => array('period' => '*/2 * * * *'),
        );
    }

}