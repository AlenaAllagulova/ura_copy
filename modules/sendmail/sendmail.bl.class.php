<?php

abstract class SendmailBase_ extends SendmailModule
{
    /** @var SendmailModel */
    public $model = null;

    public function init()
    {
        parent::init();

        $this->aTemplates['sendmail_massend'] = array(
            'title'       => _t('sendmail','Почта: массовая рассылка писем'),
            'description' => _t('sendmail','Уведомление, отправляемое при массовой рассылке'),
            'vars'        => array('{msg}' => _t('sendmail','Текст письма'))
        ,
            'impl'        => true,
            'priority'    => 1000,
            'enotify'     => Users::ENOTIFY_NEWS,
        );
    }
    
    /**
     * Получение тегов начала и конца макроса
     * @return array
     */
    public function getTags()
    {
        return array($this->tagStart, $this->tagEnd);
    }

}