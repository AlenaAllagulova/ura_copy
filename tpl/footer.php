<?php
/**
 * Футер сайта
 */
    $aFooterMenu = Sitemap::view('footer');
    $footerLink = function ($item, $extraClass = '') {
        if (!empty($item['a'])) {
            return '<li>'.$item['title'].'</li>';
        }
        return '<li><a href="'.$item['link'].'"'.($item['target'] === '_blank' ? ' target="_blank"' : '').' class="'.(!empty($item['style']) ? $item['style'] : '').(!empty($extraClass) ? ' '.$extraClass : '').'">'.$item['title'].'</a></li>';
    };
    $footerText = Site::footerText();
    $aCounters = Site::i()->getCounters();
    $languages = Site::languagesList();

?>
<!-- BEGIN footer -->
<? if(DEVICE_DESKTOP_OR_TABLET): ?>
<p class="c-scrolltop" id="j-scrolltop" style="display: none;">
    <a href="#"><span><i class="fa fa-arrow-up"></i></span><?= _t('', 'Наверх'); ?></a>
</p>
<div id="footer" class="l-footer hidden-phone">
    <div class="content">
        <div class="container-fluid  l-footer__content">
            <div class="row-fluid l-footer__content_padding">
                <div class="span4">
                    <?= config::get('copyright_'.LNG); ?>
<?  ?>
                </div>
                <div class="span2">
                    <? if( ! empty($aFooterMenu['col1']['sub']) ) { ?>
                    <ul><? foreach($aFooterMenu['col1']['sub'] as $v) {
                            echo $footerLink($v);
                           } ?>
                    </ul>
                    <? } ?>
                </div>
                <div class="span3">
                    <? if( ! empty($aFooterMenu['col2']['sub']) ) { ?>
                    <ul><? foreach($aFooterMenu['col2']['sub'] as $v) {
                            echo $footerLink($v);
                           } ?>
                    </ul>
                    <? } ?>
                </div>
                <div class="span3">
                    <? if( ! empty($aFooterMenu['col3']['sub']) ) { ?>
                    <ul><? foreach($aFooterMenu['col3']['sub'] as $v) {
                            echo $footerLink($v);
                           } ?>
                    </ul>
                    <? } ?>
                    <div class="l-footer__content__counters">
                        <?  # Выбор языка:
                        if (sizeof($languages) > 1) { ?>
                            <div class="l-footer__lang rel">
                                <?= _t('', 'Язык:') ?> <a class="dropdown-toggle ajax ajax-ico" id="j-language-dd-link" data-current="<?= LNG ?>" href="javascript:void(0);">
                                    <span class="lnk"><?= $languages[LNG]['title'] ?></span> <i class="fa fa-caret-down"></i>
                                </a>
                                <div class="dropdown-menu dropdown-block pull-left box-shadow" id="j-language-dd">
                                    <ul>
                                        <? foreach($languages as $k=>$v) { ?>
                                            <li>
                                                <a href="<?= ($k==LNG ? 'javascript:void(0);' : bff::urlLocaleChange($k)) ?>" class="ico <? if($k==LNG){ ?> active<? } ?>">
                                                    <img src="<?= bff::url('/img/lang/'.$k.'.gif') ?>" alt="" />
                                                    <span><?= $v['title'] ?></span>
                                                </a>
                                            </li>
                                        <? } ?>
                                    </ul>
                                </div>
                            </div>
                            <script type="text/javascript">
                                <? js::start() ?>
                                $(function(){
                                    app.popup('language', '#j-language-dd', '#j-language-dd-link');
                                });
                                <? js::stop() ?>
                            </script>
                        <? }
                        ?>
                        <div class="l-footer__content__counters__list">
                        <? if( ! empty($aCounters)) { ?>
                            <? foreach($aCounters as $v) { ?><div class="item"><?= $v['code'] ?></div><? } ?>
                        <? } ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <?php if (!empty($footerText)) { ?>
            <div class="l-footer_text mrgt10 mrgb10">
                <div class="container-fluid"><?= $footerText; ?></div>
            </div>
        <?php } ?>
    </div>
</div>
<? endif; ?>
<? if(DEVICE_PHONE): ?>
<div id="footer" class="l-footer l-footer_mobile visible-phone">
    <div class="l-footer_mobile__menu">
    <? if( ! empty($aFooterMenu['col1']['sub']) ) { ?>
        <ul><? foreach($aFooterMenu['col1']['sub'] as $v) {
                echo $footerLink($v);
            } ?>
        </ul>
    <? } ?>
    </div>
    <div class="l-footer_mobile__menu">
        <? if( ! empty($aFooterMenu['col2']['sub']) ) { ?>
            <ul><? foreach($aFooterMenu['col2']['sub'] as $v) {
                    echo $footerLink($v, 'pseudo-link');
                } ?>
            </ul>
        <? } ?>
    </div>
    <div class="l-footer_mobile__lang mrgt20">
    <?  # Выбор языка:
    if (sizeof($languages) > 1) { ?>
        <div class="l-footer__lang rel">
            <?= _t('', 'Язык:') ?> <a class="dropdown-toggle ajax ajax-ico" id="j-language-dd-phone-link" data-current="<?= LNG ?>" href="javascript:void(0);">
                <span class="lnk"><?= $languages[LNG]['title'] ?></span> <i class="fa fa-caret-down"></i>
            </a>
            <div class="dropdown-menu dropdown-block box-shadow" id="j-language-dd-phone">
                <ul>
                    <? foreach($languages as $k=>$v) { ?>
                        <li>
                            <a href="<?= ($k==LNG ? 'javascript:void(0);' : bff::urlLocaleChange($k)) ?>" class="ico <? if($k==LNG){ ?> active<? } ?>">
                                <img src="<?= bff::url('/img/lang/'.$k.'.gif') ?>" alt="" />
                                <span><?= $v['title'] ?></span>
                            </a>
                        </li>
                    <? } ?>
                </ul>
            </div>
        </div>
        <script type="text/javascript">
            <? js::start() ?>
            $(function(){
                app.popup('language-phone', '#j-language-dd-phone', '#j-language-dd-phone-link');
            });
            <? js::stop() ?>
        </script>
    <? }
    ?>
    </div>
    <div class="l-footer_mobile__copy mrgt15 mrgb30">
        <?= config::get('copyright_'.LNG); ?>
<?  ?>
        <br>
    </div>
    <? if ( ! empty($aCounters) && ! bff::deviceDesktopResponsive()) { ?>
    <div class="l-footer_mobile__counters mrgt20">
        <? foreach($aCounters as $v) { ?><div><?= $v['code'] ?></div><? } ?>
    </div>
    <? } ?>
    <?php if (!empty($footerText)) { ?>
        <div class="l-footer_text mrgt10 mrgb10">
            <div class="container-fluid"><?= $footerText; ?></div>
        </div>
    <?php } ?>
</div>
<? endif; ?>
<!-- END footer -->
<?= View::template('js'); ?>
<?= js::renderInline(js::POS_FOOT); ?>
<?

?>