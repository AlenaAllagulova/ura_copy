<?php namespace bff\utils;

/**
 * Класс для работы с поисковой системой Sphinx используя SphinxQL
 * @abstract
 * @version 0.1
 * @modified 22.feb.2017
 */

class Sphinx_ extends \Component
{
    /** @var \PDO объект */
    protected $pdo;

    public function init()
    {
        parent::init();

        if (!static::enabled()) return;

        try {
            $host = \config::sysAdmin('sphinx.host', '127.0.0.1', TYPE_STR);
            $port = \config::sysAdmin('sphinx.port', '9306', TYPE_STR);
            $this->pdo = new \PDO('mysql:host='.$host.';port='.$port);
        } catch (\PDOException $e) {
            $error = _t('bbs', 'Ошибка подключения к базе данных SphinxQL: [host], [msg]', array('host' => \Request::host(), 'msg' => $e->getMessage()));
            $this->errors->set($error);
        }
    }

    /**
     * Поиск Sphinx включен
     * @return mixed
     */
    public static function enabled()
    {
        return \config::sysAdmin('sphinx.enabled', false, TYPE_BOOL);
    }

    /**
     * Подготовка строки поиска
     * @param string $query строка поиска
     * @param array $options опции поиска
     * @return string
     */
    protected function prepareSearchQuery($query, & $options = array())
    {
        // http://sphinxsearch.com/docs/current.html#extended-syntax
        $empty = array ('(', ')', '|', '*', '-', '!',  "'", '&', '^'); // '"'
        $query = str_replace($empty, '', $query);

        $space = array ('@', '/', '\\', '=', '~', '$', "\x00", "\n", "\r", "\x1a", "\t");
        $query = str_replace($space, ' ', $query);

        if (mb_strpos($query, '"') === 0) { # поиск точной фразы
            // http://sphinxsearch.com/blog/2013/07/23/from-api-to-sphinxql-and-back-again/
            $options['ranker'] = 'proximity';
        } else {
            $query = str_replace('"', '', $query);
            # фомируем запрос поиска каждого слова отдельно
            $words = explode(' ', $query);
            $tmp = array();
            foreach ($words as $word) {
                if (mb_strlen($word) >= 3) {
                    $tmp[] = "($word | $word*)";
                }
            }
            $query = join(' & ', $tmp);
        }
        return $query;
    }

    /**
     * Обрабатываем (выполняем) SQL запросы
     * @param string|array $query запросы
     * @param array $bind аргументы
     * @param integer|boolean $fetchType PDO::FETCH_NUM, PDO::FETCH_ASSOC, PDO::FETCH_BOTH, PDO::FETCH_OBJ
     * @param string $fetchFunc
     * @param array $prepareOptions
     * @return array
     */
    protected function exec($query, array $bind = null, $fetchType = false, $fetchFunc = 'fetchAll', array $prepareOptions = array())
    {
        if (!$this->pdo) return false;

        $batch = is_array($query);
        if ($batch) {
            if (is_null($bind)) {
                $bind = array();
                for ($i = 0; $i < count($query); $i++) {
                    $bind[] = null;
                }
            }
        } else {
            $query = array($query);
            $bind = array($bind);
        }

        $result = false;
        foreach (array_combine($query, $bind) as $cmd => $arg)
        {
            if (is_null($arg)) {
                $query = $this->pdo->query($cmd);
            } else {
                $query = $this->pdo->prepare($cmd, $prepareOptions);
                if (is_object($query)) {
                    foreach ($arg as $key => $value) {
                        if (!(is_array($value) ?
                            $query->bindValue($key, $value[0], $value[1]) :
                            $query->bindValue($key, $value, $this->db->type($value)))
                        ) {
                            break;
                        }
                    }
                    $query->execute();
                }
            }
            # Проверяем SQLSTATE на наличие ошибок
            foreach (array($this->pdo, $query) as $obj) {
                if ($obj !== false && $obj->errorCode() != \PDO::ERR_NONE) {
                    $error = $obj->errorInfo();
                    \bff::log('[SphinxQL Error] ( ' . (@$error[0] . '.' . (isset($error[1]) ? $error[1] : '?')) . ' : ' . (isset($error[2]) ? $error[2] : '') . ' )');
                    return false;
                }
            }

            if ($fetchFunc !== false) {
                $result = $query->$fetchFunc($fetchType);
            } else {
                $result = $query;
            }
        }

        return $result;
    }

    /**
     * Получаем несколько строк из таблицы
     * @param string $query текст запроса
     * @param array|null $bindParams параметры запроса или null
     * @param integer $fetchType PDO::FETCH_NUM, PDO::FETCH_ASSOC, PDO::FETCH_BOTH, PDO::FETCH_OBJ
     * @param string $fetchFunc
     * @return mixed @see self::exec @method
     */
    protected function select($query, $bindParams = null, $fetchType = \PDO::FETCH_ASSOC, $fetchFunc = 'fetchAll')
    {
        return $this->exec($query, $bindParams, $fetchType, $fetchFunc);
    }

}