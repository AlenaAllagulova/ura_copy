<?php

define('BFF_ADMINPANEL', 1);
require __DIR__.'/../../bff.php';



Site::adminPanel(array(
    'bbs'=>_t('menu','Объявления'), 'shops'=>_t('menu','Магазины'),
    'users'=>_t('menu','Пользователи'), 'bills'=>_t('menu','Счета'),
    'banners'=>_t('menu','Баннеры'), 'internalmail'=>_t('menu','Сообщения'),
    'blog'=>_t('menu','Блог'), 'help'=>_t('menu','Помощь'),
    'pages'=>_t('menu','Страницы'), 'contacts'=>_t('menu','Контакты'),
    'sendmail'=>_t('menu','Работа с почтой'), 'sitemap'=>_t('menu','Карта сайта и меню'),
    'seo'=>_t('','SEO'), 'settings'=>_t('menu','Настройки сайта'),
));