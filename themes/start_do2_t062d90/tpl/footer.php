<?php
/**
 * Футер сайта
 */
if (config::get('tpl.footer.hide', false, TYPE_BOOL)) {
  return;
}
$footerMenu = Sitemap::view('footer');
$footerLink = function ($item, $extraClass = '') {
  if (!empty($item['a'])) {
    return '<li>' . $item['title'] . '</li>';
  }
  return '<li><a href="' . $item['link'] . '"' . ($item['target'] === '_blank' ? ' target="_blank"' : '') . ' class="' . (!empty($item['style']) ? $item['style'] : '') . (!empty($extraClass) ? ' ' . $extraClass : '') . '">' . $item['title'] . '</a></li>';
};
$footerText = Site::footerText();
$counters = Site::i()->getCounters();
$languages = Site::languagesList();

?>
<!-- BEGIN footer -->
<?php if (DEVICE_DESKTOP_OR_TABLET) { ?>
  <div class="l-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-3">
          <?= config::get('copyright_' . LNG); ?>
        </div>
        <?php if (!empty($footerMenu['col1']['sub'])) { ?>
          <div class="col-sm-2">
            <ul class="l-footer-menu">
              <?php foreach ($footerMenu['col1']['sub'] as $v) {
                echo $footerLink($v);
              } ?>
            </ul>
          </div>
        <?php } ?>
        <?php if (!empty($footerMenu['col2']['sub'])) { ?>
          <div class="col-sm-2">
            <ul class="l-footer-menu">
              <?php foreach ($footerMenu['col2']['sub'] as $v) {
                echo $footerLink($v);
              } ?>
            </ul>
          </div>
        <?php } ?>
        <?php if (!empty($footerMenu['col3']['sub'])) { ?>
          <div class="col-sm-2">
            <ul class="l-footer-menu">
              <?php foreach ($footerMenu['col3']['sub'] as $v) {
                echo $footerLink($v);
              } ?>
            </ul>
          </div>
        <?php } ?>
        <div class="col-sm-3">
          <?php # Выбор языка:
          if (sizeof($languages) > 1) { ?>
            <div class="l-footer-lang dropdown">
              <?= _t('', 'Язык:') ?>
              <a data-current="<?= LNG ?>" href="#" data-toggle="dropdown">
                <?= $languages[LNG]['title'] ?> <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <?php foreach ($languages as $k => $v) { ?>
                  <li>
                    <a href="<?= ($k==LNG ? 'javascript:void(0);' : bff::urlLocaleChange($k)) ?>" class="ico <?php if($k==LNG){ ?> active<?php } ?>">
                      <img src="<?= bff::url('/img/lang/' . $k . '.gif') ?>" alt=""/>
                      <span><?= $v['title'] ?></span>
                    </a>
                  </li>
                <?php } ?>
              </ul>
            </div>
            <script type="text/javascript">
              <?php js::start() ?>
              $(function () {
                app.popup('language', '#j-language-dd', '#j-language-dd-link');
              });
              <?php js::stop() ?>
            </script>
          <?php } ?>
          <div class="l-footer-counters">
            <?php if (!empty($counters)) { ?>
              <?php foreach ($counters as $v) { ?>
                <div class="item"><?= $v['code'] ?></div><?php } ?>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>
<?php if (!empty($footerText)) { ?>
  <div class="l-footer-text">
    <div class="container">
      <?= $footerText; ?>
    </div>
  </div>
<?php } ?>
<!-- Scripts -->
<?= View::template('js'); ?>
<?= js::renderInline(js::POS_FOOT); ?>
<script>
  // Tooltips and popovers
  $(document).ready(function () {
    $('.has-tooltip').tooltip();
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
  });
</script>